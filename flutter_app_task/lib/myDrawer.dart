import 'package:flutter/material.dart';

class drawerWidget extends StatelessWidget{

  TextStyle Text_Style(){
    return TextStyle(
      fontSize: 22,
      color: Colors.white,
    );
  }

  Widget build(BuildContext context){
    return Container(
      color: Colors.blue,
      child: ListView(
        children: <Widget>[
          Container(
            height: 120,
            margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.2),
            alignment: Alignment.centerLeft,
            child: Image.asset('assets/logo2.png',height: 80,),
          ),
          Container(
            child: ListTile(
              title: Text("home",
                style: Text_Style(),
              ),
              onTap: (){},
            ),
          ),
          Container(
            child: ListTile(
              title: Text("Profile",
                style: Text_Style(),
              ),
              onTap: (){},
            ),
          ),
          Container(
            child: ListTile(
              title: Text("Settings",
                style: Text_Style(),
              ),
              onTap: (){},
            ),
          ),
        ],
      ),
    );
  }
}