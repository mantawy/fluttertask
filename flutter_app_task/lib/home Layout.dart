import 'package:flutter/material.dart';
import 'myDrawer.dart';
import 'ui/home_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CustomeDrawer extends StatefulWidget{
  CustomDrawerState createState()=> CustomDrawerState();
}
class CustomDrawerState extends State<CustomeDrawer>
    with SingleTickerProviderStateMixin{

  AnimationController animationController;

  @override
  void initState(){
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 240),
    );
  }
  void toggle() => animationController.isDismissed
      ?animationController.forward()
      :animationController.reverse();


  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey();

  @override
  Widget build(BuildContext context) {
    final maxSlide = 225.0;

    var myChild = Scaffold(
      key: _scaffoldkey,
      appBar: AppBar(
        title: Text("Task app"),
        leading: StatefulBuilder(
          builder: (BuildContext context, setState) {
            return IconButton(
              icon: Icon(Icons.format_align_left),
              onPressed: toggle,
            );
          },
        ),
      ),
      drawer: Drawer(),
      body: HomePage(),
    );

    return AnimatedBuilder(
      animation: animationController,
      builder: (context,_){
        double slide = maxSlide *animationController.value;
        double scale = 1 - (animationController.value * 0.3);
        return Stack(
          children: <Widget>[
            drawerWidget(),
            Transform(
              transform: Matrix4.identity()
                ..translate(slide)
                ..scale(scale),
              alignment: Alignment.centerLeft,
              child: myChild,
            ),
          ],
        );
      },
    );
  }
}