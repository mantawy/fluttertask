import 'package:flutter_app_task/data/api_result_model.dart';
import 'package:meta/meta.dart';

abstract class ArticleState {}

class ArticleInitialState extends ArticleState {
  @override
  // TODO: implement props
  List<Object> get states => [];
}

class ArticleLoadingState extends ArticleState {
  @override
  // TODO: implement props
  List<Object> get states => [];
}

class ArticleLoadedState extends ArticleState {

  List<Articles> articles;

  ArticleLoadedState({@required this.articles});

  @override
  // TODO: implement props
  List<Object> get states => [articles];
}

class ArticleErrorState extends ArticleState {

  String message;

  ArticleErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get states => [message];
}