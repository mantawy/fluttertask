import 'package:flutter_app_task/data/api_result_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';


abstract class DataRepository {
  Future<List<Articles>> getArticles();
}

class DataRepositoryImpl implements DataRepository {

  @override
  Future<List<Articles>> getArticles() async {
    var response = await http.get(
        'http://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=a0860378c49a43989dfd1c54327f0d82');
    if (response.statusCode == 200) {
      var data = json.decode(response.body);
      List<Articles> articles = ApiResultModel.fromJson(data).articles;
      return articles;
    } else {
      print(response.statusCode.toString());
      throw Exception();
    }
  }

}