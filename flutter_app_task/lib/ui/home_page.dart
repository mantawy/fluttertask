import 'package:flutter_app_task/block/block.dart';
import 'package:flutter_app_task/block/event.dart';
import 'package:flutter_app_task/block/state.dart';
import 'package:flutter_app_task/data/api_result_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:cached_network_image/cached_network_image.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ArticleBloc articleBloc;

  @override
  void initState() {
    super.initState();
    articleBloc = BlocProvider.of<ArticleBloc>(this.context);
    articleBloc.add(FetchEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocListener<ArticleBloc, ArticleState>(
        listener: (context, state) {
          if (state is ArticleErrorState) {
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Text(state.message),
              ),
            );
          }
        },
        child: BlocBuilder<ArticleBloc, ArticleState>(
          builder: (context, state) {
            if (state is ArticleInitialState) {
              return buildLoading();
            } else if (state is ArticleLoadingState) {
              return buildLoading();
            } else if (state is ArticleLoadedState) {
              return buildArticleList(state.articles);
            } else if (state is ArticleErrorState) {
              return buildErrorUi(state.message);
            }
          },
        ),
      ),
    );
  }
  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  Widget buildArticleList(List<Articles> articles) {
    final mediaQuery = MediaQuery.of(context);
    int count = (((mediaQuery.size.width-10)/380)).round();
    int deviceShortestSide = mediaQuery.size.shortestSide.round();
    int cardHei;
    if(deviceShortestSide > 415){
      cardHei = 300;
    }
    else {
      cardHei = 250;
    }
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: count,
        childAspectRatio:// 1,
        (((mediaQuery.size.width-(10*count))/count) /cardHei),
      ),
      itemCount: articles.length,
      itemBuilder: (ctx, pos) {
        return InkWell(
          onTap: (){},
          child: Container(
            margin: EdgeInsets.all(5),
            padding: EdgeInsets.all(0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: Colors.white,
                border: Border.all(
                  width: 1,
                  color: Colors.black12,
                )
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        width: mediaQuery.size.width,
                        child: Hero(
                          tag: articles[pos].urlToImage,
                          child: CachedNetworkImage(
                            imageUrl: articles[pos].urlToImage,
                            placeholder: (context, url) => Center(
                              child: CircularProgressIndicator(),),
                            fit: BoxFit.cover,
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height,
                          ),
                        ),
                      ),
                      flex: 5,
                    ),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(top: 0,right: 8,left: 8),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(padding: EdgeInsets.all(6)),
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Expanded(
                                  flex: 7,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(articles[pos].title,
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: Colors.black,
                                          fontWeight: FontWeight.normal,
                                          height: 1,
                                        ),
                                        maxLines: 2,
                                        textAlign: TextAlign.start,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ), //Meal title & cal count
                            Padding(padding: EdgeInsets.all(5)),
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text("Auther: "+articles[pos].author,
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black,
                                    fontWeight: FontWeight.normal,
                                    height: 1,
                                  ),
                                  maxLines: 2,
                                  textAlign: TextAlign.start,
                                ),
                              ],
                            ),
                            Padding(padding: EdgeInsets.all(5)),
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(articles[pos].publishedAt,
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.normal,
                                    height: 1,
                                  ),
                                  maxLines: 2,
                                  textAlign: TextAlign.start,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      flex: 4,
                    )
                  ],
                ),
              ),
            ),
          ),
        );
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: InkWell(
            child: ListTile(
              leading: ClipOval(
                child: Hero(
                  tag: articles[pos].urlToImage,
                  child: Image.network(
                    articles[pos].urlToImage,
                    fit: BoxFit.cover,
                    height: 70.0,
                    width: 70.0,
                  ),
                ),
              ),
              title: Text(articles[pos].title),
              subtitle: Text(articles[pos].publishedAt),
            ),
            onTap: () {},
          ),
        );
      },
    );
  }

}